// TASK 1

let userInput = prompt("Enter your age");

if (userInput === null || userInput.trim() === '') {
    alert("You were supposed to enter your age.");

} else if (!isNaN(userInput)) {
    let age = parseInt(userInput);

    if (age < 0) {
        alert("Don't bullshit a bullshitter.");
    } else if (age < 12) {
        alert("You are a child");
    } else if (age < 18) {
        alert("You are a teenager");
    } else if (age >= 18 && age <= 100) {
        alert("You are an adult.");
    } else {
        alert("Don't bullshit a bullshitter.");
    }
} else {
    alert("Numbers only.");
}

// TASK 2

let month = prompt("Введіть місяць року (українською):").toLowerCase();

switch (month) {
    case "січень":
    case "березень":
    case "травень":
    case "липень":
    case "серпень":
    case "жовтень":
    case "грудень":
        console.log(`У місяці ${month} 31 день.`);
        break;
    case "квітень":
    case "червень":
    case "вересень":
    case "листопад":
        console.log(`У місяці ${month} 30 днів.`);
        break;
    case "лютий":
        console.log(`У місяці ${month} 28 або 29 днів.`);
        break;
    default:
        console.log("Невірно введений місяць.");
}











